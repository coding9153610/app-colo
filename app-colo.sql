-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le : dim. 16 juin 2024 à 19:31
-- Version du serveur : 5.7.39
-- Version de PHP : 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `app-colo`
--

-- --------------------------------------------------------

--
-- Structure de la table `expenses`
--

CREATE TABLE `expenses` (
  `id` int(11) NOT NULL,
  `expense_name` varchar(100) NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `expense_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expense_sharing` enum('equal','proportionnal','individual') NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `flatmates`
--

CREATE TABLE `flatmates` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `flatshare_id` int(11) NOT NULL,
  `registration_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `flatmates`
--

INSERT INTO `flatmates` (`id`, `user_id`, `flatshare_id`, `registration_date`) VALUES
(1, 53, 21, '2024-06-08 11:05:50'),
(2, 55, 23, '2024-06-08 12:28:01'),
(3, 57, 24, '2024-06-08 14:15:22'),
(4, 58, 25, '2024-06-08 14:21:18'),
(7, 61, 46, '2024-06-09 14:26:20'),
(8, 62, 47, '2024-06-09 14:32:43'),
(13, 62, 23, '2024-06-09 21:49:03'),
(15, 66, 23, '2024-06-09 21:57:21'),
(16, 67, 48, '2024-06-09 22:05:30'),
(17, 68, 23, '2024-06-10 16:09:28'),
(18, 124, 23, '2024-06-12 09:51:05'),
(19, 125, 23, '2024-06-12 21:39:46'),
(20, 126, 23, '2024-06-12 22:27:04'),
(21, 55, 49, '2024-06-14 09:15:01'),
(22, 55, 49, '2024-06-14 09:16:18'),
(23, 131, 50, '2024-06-14 17:20:49'),
(24, 132, 50, '2024-06-14 17:25:32'),
(25, 133, 51, '2024-06-15 21:30:05');

-- --------------------------------------------------------

--
-- Structure de la table `flatshares`
--

CREATE TABLE `flatshares` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `creation_date_flatshare` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `address` varchar(100) NOT NULL,
  `access_code` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `flatshares`
--

INSERT INTO `flatshares` (`id`, `name`, `creation_date_flatshare`, `address`, `access_code`) VALUES
(1, 'My Flatshare', '2024-06-07 15:44:34', '123 Main St', 'U7OMH4'),
(21, 'Angelica Turner', '2024-06-08 09:05:50', 'Corporis aut quia re', 'VUNMPY'),
(23, 'Coda', '2024-06-08 10:28:01', '24 Rue Jeanne d Arc, 45000 Orléans', '2RD109'),
(24, 'Coloc', '2024-06-08 12:15:22', 'Rue du poirier rond', 'GBWS0E'),
(25, 'Test', '2024-06-08 12:21:18', 'test', 'MLBAZH'),
(46, 'Sneakers Homme', '2024-06-09 12:26:20', 'Rue Jeanne d&#039;Arc', '4LG5LA'),
(47, 'blabla', '2024-06-09 12:32:43', 'blabla', '8ZK83R'),
(48, 'Calak', '2024-06-09 20:05:30', 'Rue du poirier rond', '2O8GW8'),
(49, 'Buddy', '2024-06-14 07:15:01', '150 rue des chateignes', 'RQ2M1Z'),
(50, 'Final Test ', '2024-06-14 15:20:49', 'route de Beaune', '9U30IZ'),
(51, 'Afpa', '2024-06-15 19:30:05', 'Propriete de l&#039;Archette, Rue Basse Mouillère, 45160 Olivet', 'SK1IH6');

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `content` text NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL,
  `task_name` varchar(50) NOT NULL,
  `task_description` text NOT NULL,
  `deadline_spot` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `task_priority` enum('basse','moyenne','haute') NOT NULL,
  `task_category` enum('menage','courses','cuisine','administratif','bricolage','divers') NOT NULL,
  `status` enum('a faire','en cours','terminee') NOT NULL,
  `user_id` int(11) NOT NULL,
  `flatmate_id` int(11) NOT NULL,
  `flatshare_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tasks`
--

INSERT INTO `tasks` (`id`, `task_name`, `task_description`, `deadline_spot`, `task_priority`, `task_category`, `status`, `user_id`, `flatmate_id`, `flatshare_id`) VALUES
(1, 'Clean the kitchen', 'Clean all surfaces and mop the floor', '2024-06-14 10:12:43', 'haute', 'menage', 'a faire', 57, 3, 24),
(2, 'Nettoyage de la cuisine', 'Nettoyer la cuisine après le dîner.', '2023-06-29 22:00:00', 'haute', 'menage', 'a faire', 55, 2, 23);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `password`, `created_at`) VALUES
(53, 'Lois', 'Mckay', 'wamavo@mailinator.com', '$2y$10$sESFY0TcmkGUqMWBYeu8HuFvMYCmTpsRrgP91cYXa978ezXmATDnG', '2024-06-08 10:13:38'),
(55, 'David', 'Chatton', 'davidchatton.dev@gmail.com', '$2y$10$xWSLAEX2DW.MyJ75K.7ADu/bIOko3t5K2OGEY1WOuryA4GsUTlaiS', '2024-06-08 12:27:06'),
(57, 'Alea', 'Cote', 'niqovugaz@mailinator.com', '$2y$10$zXjNE67NgQxjUWKGpm4nOOu1JPKuv2MD1Mwdvk/fcMyJDJH8U57eC', '2024-06-08 14:14:34'),
(58, 'Barrett', 'Odonnell', 'test@mailinator.com', '$2y$10$IsiBj3Z5qaVLg0nszqmwX.JETtDajnI7QavSQSHvdrUCye9X0TKRu', '2024-06-08 14:20:50'),
(61, 'Quail', 'Fredo', 'test@gmail.com', '$2y$10$fRHyPpceGbXo0Br7svHWuenZXYESS5klHP9Hk0xpxQ4sh0RqvbcnW', '2024-06-16 00:43:18'),
(62, 'Cyrus', 'Andrew', 'viqadowi@mailinator.com', '$2y$10$aV/hk6ic4LIgVS5BM9NOa.4QwGxgsAP9oXYmeNKB9A/dvk89GfawO', '2024-06-16 00:58:20'),
(66, 'Madeson', 'Cote', 'tuze@mailinator.com', '$2y$10$VVZLz4t5FzQacYdhcxUboeN/SiOHrP7HCG8v.Bnls2kxh3nzZB2Oe', '2024-06-09 16:57:28'),
(67, 'Melyssa', 'Chatton', 'newemail@example.com', '$2y$10$aJB7liF0mugYGHmBdKdAh.XqZC/LjImZTskCZFbB7kD3NJtfQ5Jz.', '2024-06-12 14:11:01'),
(68, 'James', 'Cohen', 'momosis@mailinator.com', '$2y$10$Fj4xd/zQpxyc6J7CDxbId.o5gJx.sza/DqInWCJH7eRLDw4dfdDyq', '2024-06-10 16:05:26'),
(124, 'Shay', '', '', '$2y$10$AG3RVspD0WkAQUG4iVPjZedfFrSQUkW3F1QkjCPP177ZSQ5ZalGrC', '2024-06-12 21:34:34'),
(125, 'Hyatt', 'Chatton', 'newemail@example.com', '$2y$10$GTLCiuDi0gvo/WkHKj.areZT0aEmPiBk5hsOCVwAoslYFKfJHqTdm', '2024-06-12 22:21:41'),
(126, 'Aaron', 'Snow', 'gybucumude@mailinator.com', '$2y$10$EBPLdMg.IYimUPKrxQmuweEvnvsjOog0vQNzIj4eXORWX9vSoXWxu', '2024-06-12 22:26:49'),
(131, 'Cleo', 'Carney', 'huhifux@mailinator.com', '$2y$10$f26.72UUct7AxL3uBGaQP.L7gfBjERGBIObkQl5RUijmI2ymyrnby', '2024-06-14 17:20:03'),
(132, 'Baxter', 'Hart', 'muvewire@mailinator.com', '$2y$10$5eJr4Q6Cd5KcddpA2EaZBOUaWPcElt4rQWcj7ml9gSh1eVXqUBNYO', '2024-06-14 17:24:35'),
(133, 'Thierry', 'Chatton', 'thierrychatton@wanadoo.fr', '$2y$10$tLYhAc65sSr2EIcKH3WIROxXi69DYyyciO60tsJd1Hso6c.O.kp0.', '2024-06-15 22:21:32');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `flatmates`
--
ALTER TABLE `flatmates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `flatmates_users_foreign` (`user_id`),
  ADD KEY `flatmates_flatshare_foreign` (`flatshare_id`);

--
-- Index pour la table `flatshares`
--
ALTER TABLE `flatshares`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`) USING BTREE;

--
-- Index pour la table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `messages_user_id_foreign` (`user_id`);

--
-- Index pour la table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tasks_flatmate_id` (`flatmate_id`),
  ADD KEY `fk_tasks_user_id` (`user_id`),
  ADD KEY `fk_tasks_flatshare_id` (`flatshare_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `flatmates`
--
ALTER TABLE `flatmates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT pour la table `flatshares`
--
ALTER TABLE `flatshares`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT pour la table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `flatmates`
--
ALTER TABLE `flatmates`
  ADD CONSTRAINT `flatmates_flatshare_foreign` FOREIGN KEY (`flatshare_id`) REFERENCES `flatshares` (`id`),
  ADD CONSTRAINT `flatmates_users_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `fk_tasks_flatmate_id` FOREIGN KEY (`flatmate_id`) REFERENCES `flatmates` (`id`),
  ADD CONSTRAINT `fk_tasks_flatshare_id` FOREIGN KEY (`flatshare_id`) REFERENCES `flatshares` (`id`),
  ADD CONSTRAINT `fk_tasks_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
